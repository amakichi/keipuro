package log.paiza.C046;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

// C046 at 2018/02/08

public class Main {
	public static void main(String[] args) {
		Main main = new Main();
		main.solveC();
	}

	private void solveC() {
		Scanner sc = new Scanner(System.in);
		int p = Integer.parseInt(sc.nextLine());
		List<String> names = new ArrayList<>(Arrays.asList(sc.nextLine().split(" ")));
		int b = Integer.parseInt(sc.nextLine());
		Map<String, Integer> m = names.stream().collect(Collectors.toMap(s -> s, s -> 0));
		for (int i = 0; i < b; i++) {
			String data[] = sc.nextLine().split(" ");
			if (m.containsKey(data[0])) {
				m.put(data[0], m.get(data[0]) + Integer.parseInt(data[1]));
			}
		}
		m.entrySet().stream()
			.sorted(java.util.Collections.reverseOrder(java.util.Map.Entry.comparingByValue()))
			.forEach(s -> System.out.println(s.getKey()));
    }
}