package log.paiza.D086;

import java.util.Scanner;

//D086 at 2018/01/26

public class Main {
	public static void main(String[] args) {
		Main main = new Main();
		main.solveD();
	}

	private void solveD() {
        Scanner sc = new Scanner(System.in);
        int line = Integer.parseInt(sc.nextLine());
        System.out.println(line * 5);
    }
}