package log.paiza.D007;

import java.util.Scanner;

//D007 at 2018/01/26

public class Main {
	public static void main(String[] args) {
		Main main = new Main();
		main.solveD();
	}

	private void solveD() {
        Scanner sc = new Scanner(System.in);
        String line = sc.nextLine();
        int max = Integer.parseInt(line);
        for (int i = 0; i < max; i++) {
            System.out.print("*");
        }
    }
}