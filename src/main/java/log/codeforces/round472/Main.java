package log.codeforces.round472;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

// codeforces round 472 at 2018/03/25

public class Main {
    public static void main(String[] args) {
        Main main = new Main();
        main.solveA();
    }

    private void solveA() {
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();
        sc.nextLine();
        boolean flg = false;
        List<String> colors = Arrays.asList(sc.nextLine().split(""));
        int q = colors.indexOf("?");
        String past = "";
        for (int i = 0; i < colors.size(); i++) {
            String now = colors.get(i);
            if (i != 0) {
                if (past.equals(now)) {
                    System.out.println("No");
                    return;
                }
            }
            past = now.equals("?") ? "" : now;
        }
        if (q >= 0) {
            if (q == 0 || q == colors.size()) {
                flg = true;
            } else if (colors.get(q + 1).equals(colors.get(q - 1))
                    || colors.get(q + 1).equals("?")) {
                flg = true;
            }
        }

        if (flg) {
            System.out.println("Yes");
        } else {
            System.out.println("No");
        }
    }

    private void solveB() {
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();
        System.out.println(N);
    }

    private void solveC() {
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();
        System.out.println(N);
    }

    private void solveD() {
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();
        System.out.println(N);
    }

    private void solveE() {
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();
        System.out.println(N);
    }

    private void solveF() {
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();
        System.out.println(N);
    }

}