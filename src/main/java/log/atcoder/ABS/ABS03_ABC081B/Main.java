package log.atcoder.ABS.ABS03_ABC081B;

import java.util.Scanner;
import java.util.stream.Stream;

// ABS_ABC081B at 2018/03/20

public class Main {
    public static void main(String[] args) {
        Main main = new Main();
        main.solveB();
    }

    private void solveA() {
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();
        System.out.println(N);
    }

    private void solveB() {
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();
        sc.nextLine();
        int[] nums = Stream.of(sc.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray();
        int cnt = 0;
        Boolean con = true;
        while(con) {
            for (int i = 0; i < nums.length; i++) {
                if (nums[i] % 2 == 0) {
                    nums[i] = nums[i] / 2;
                } else {
                    con = false;
                }
            }
            if (con) {
                cnt++;
            }
        }
        System.out.println(cnt);
    }

    private void solveC() {
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();
        System.out.println(N);
    }

    private void solveD() {
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();
        System.out.println(N);
    }

    private void solveE() {
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();
        System.out.println(N);
    }

    private void solveF() {
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();
        System.out.println(N);
    }

}